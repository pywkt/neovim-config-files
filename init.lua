require("core.options")
require("core.keymaps")
require("lazy-setup")
require("core.autocommands")

if vim.g.neovide then
  require("neovide.config")
end

local M = {}

M.servers = {
  "lua_ls",
  "cssls",
  "html",
  "tsserver",
  "eslint",
  "bashls",
  "jsonls",
  "yamlls",
  "tailwindcss",
}

M.linters = {
  "prettier",
  "stylua",
}

M.parsers = {
  "lua",
  "vim",
  "markdown",
  "markdown_inline",
  "bash",
  "python",
  "javascript",
  "typescript",
  "tsx",
  "html",
  "css",
  "json",
  "yaml",
  "toml",
  "regex",
  "dockerfile",
}

return M

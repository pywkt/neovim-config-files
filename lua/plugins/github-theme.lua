local palettes = {
  all = {
    bg0 = "#2c2c2c",
    bg1 = "#2c2c2c",
    bg2 = "#2c2c2c",
    bg3 = "#2c2c2c",
  },
}

return {
  "projekt0n/github-nvim-theme",
  enabled = true,
  lazy = false,   -- make sure we load this during startup if it is your main colorscheme
  priority = 1000, -- make sure to load this before all the other start plugins
  opts = {
    -- options = {
    -- styles = {
    --   comments = "italic",
    --   keywords = "italic",
    -- },
    -- },
  },
  config = function()
    require("github-theme").setup({
      palettes = palettes,
      -- palettes = {
      --   github_dark_dimmed = {
      --     bg1 = "#2c2c2c",
      --     bg0 = "#ffa332",
      --     bg3 = "#00ffaa",
      --     sel0 = "#aa0382",
      --   },
      -- },
      -- groups = groups,
      options = {
        styles = {
          comments = "italic",
          keywords = "italic",
          conditionals = "italic",
        },
        hide_nc_statusline = false,
        inverse = {
          match_paren = true,
          visual = true,
          search = true,
        },
        darken = {
          sidebars = {
            enabled = false,
          },
        },
      },
      groups = {
        all = {
          ["@label.jsonc"] = { link = "@label.json" },
        },
      },
    })

    vim.g.github_theme_debug = true

    -- vim.cmd("colorscheme github_dark_dimmed")
    -- require("github-theme").setup({
    --   options = {
    --     transparent = true,
    --     styles = {
    --       comments = "italic",
    --       keywords = "italic",
    --       functions = 'italic',
    --       conditionals = 'italic'
    --     },
    --   },
    -- })
    --
    -- vim.cmd("colorscheme github_dark")
  end,
}

return {
	"kdheepak/lazygit.nvim",
  lazy = false,
  priority = 1000,
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope.nvim",
	},
	config = function()
		require("telescope").load_extension("lazygit")
	end,
}

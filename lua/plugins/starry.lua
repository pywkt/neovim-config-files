return {
	"ray-x/starry.nvim",
	enabled = true,
	lazy = false, -- make sure we load this during startup if it is your main colorscheme
	priority = 1000, -- make sure to load this before all the other start plugins
	config = function()
		require("starry").setup({})
		-- vim.cmd("colorscheme earlysummer")
	end,
}

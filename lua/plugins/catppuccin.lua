return {
  "catppuccin/nvim",
  lazy = false,
  priority = 1000,
  name = "catppuccin",
  config = function()
    -- Using protected call
    local status_ok, catppuccin = pcall(require, "catppuccin")
    if not status_ok then
      return
    end

    catppuccin.setup({
      flavour = "mocha",
      no_italic = false,
      no_underline = false,
      no_bold = true,
      -- color_overrides = {
        -- mocha = {
          -- base = "#0C0C1B",
          -- mantle = "#101022"
        -- },
      -- },
      styles = {
        keywords = { "italic" },
        variables = { "italic" },
        types = { "italic" },
      },
      integrations = {
        alpha = true,
        cmp = true,
        gitsigns = true,
        illuminate = true,
        indent_blankline = { enabled = true },
        lsp_trouble = true,
        mason = true,
        mini = true,
        native_lsp = {
          enabled = true,
          underlines = {
            errors = { "undercurl" },
            hints = { "undercurl" },
            warnings = { "undercurl" },
            information = { "undercurl" },
          },
        },
        navic = { enabled = true, custom_bg = "lualine" },
        neotest = true,
        noice = true,
        notify = true,
        neotree = true,
        semantic_tokens = true,
        telescope = true,
        treesitter = true,
        which_key = true,
      },
    })

    vim.cmd.colorscheme("catppuccin")
  end,
}

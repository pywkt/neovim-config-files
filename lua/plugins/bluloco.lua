return {
	"uloco/bluloco.nvim",
	lazy = false,
	priority = 1000,
	dependencies = { "rktjmp/lush.nvim" },
	config = function()
		-- Using protected call
		local status_ok, bluloco = pcall(require, "bluloco")
		if not status_ok then
			return
		end
		bluloco.setup({
			style = "dark", -- "auto" | "dark" | "light"
			transparent = false,
			italics = true,
			terminal = vim.fn.has("gui_running") == 1, -- bluoco colors are enabled in gui terminals per default.
			guicursor = true,
		})

		-- vim.cmd.colorscheme("bluloco")
	end,
}

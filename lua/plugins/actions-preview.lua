return {
  "aznhe21/actions-preview.nvim",
  event = "VeryLazy",

  config = function()
    local status_ok, actions = pcall(require, "actions-preview")
    -- vim.keymap.set({ "n", "v"}, "<leader>aa", require("actions-preview").code_actions)
    if not status_ok then
      return
    end
    actions.setup({
      telescope = {
        sorting_strategy = "ascending",
        layout_strategy = "vertical",
        layout_config = {
          width = 0.8,
          height = 0.9,
          prompt_position = "top",
          preview_cutoff = 20,
          preview_height = function(_, _, max_lines)
            return max_lines - 15
          end,
        },
      },
    })

    vim.keymap.set({ "n", "v"}, "<leader>aa", require("actions-preview").code_actions)
  end,
}

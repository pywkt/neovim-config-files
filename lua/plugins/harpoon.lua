return {
	"ThePrimeagen/harpoon",
	config = function()
		require("harpoon").setup({})
	end,
	keys = {
		{ "<leader>ma", ":lua require('harpoon.mark').add_file()<CR>", desc = "harpoon mark file" },
		{ "<leader>m.", ":lua require('harpoon.ui').nav_next()<CR>", desc = "harpoon next file" },
		{ "<leader>m,", ":lua require('harpoon.ui').nav_prev()<CR>", desc = "harpoon prev file" },
		{ "<leader>mv", ":lua require('harpoon.ui').toggle_quick_menu()<CR>", desc = "harpoon show marks" },
	},
}

return {
	"windwp/nvim-ts-autotag",
	filetypes = {
		"html",
		"javascript",
		"javascriptreact",
		"typescript",
		"typescriptreact",
		"svelte",
		"vue",
	},
	opts = { enable_close_on_slash = false },
}

return {
  "rebelot/kanagawa.nvim",
  name = "kanagawaa",
  lazy = false,   -- make sure we load this during startup if it is your main colorscheme
  priority = 1000, -- make sure to load this before all the other start plugins
  config = function()
    -- Using protected call
    local status_ok, kanagawa = pcall(require, "kanagawa")
    if not status_ok then
      return
    end
    kanagawa.setup({
      compile = false, -- enable compiling the colorscheme
      undercurl = true, -- enable undercurls
      commentStyle = { italic = true },
      functionStyle = { italic = true },
      keywordStyle = { italic = true },
      statementStyle = { bold = true },
      typeStyle = {},
      transparent = false, -- do not set background color
      dimInactive = false, -- dim inactive window `:h hl-NormalNC`
      terminalColors = true, -- define vim.g.terminal_color_{0,17}
      colors = {          -- add/modify theme and palette colors
        palette = {
          sumiInk0 = "#fff",
          sumiInk1 = "#ffaaaa",
          sumiInk2 = "#000fff",
          sumiInk3 = "#1affff",
          sumiInk4 = "#aaaaff",
          bg_gutter = "none",
        },
        theme = {
          wave = {},
          lotus = {},
          dragon = {
            -- sumiInk0 = "#fff",
            -- sumiInk1 = "#ffaaaa",
            -- sumiInk2 = "#000fff",
            -- sumiInk3 = "#1affff",
            -- sumiInk4 = "#aaaaff",
            -- bg_gutter = "#FFFFFF",
          },
          -- all = {
          --   syn = {
          --     comment = "#4e5260",
          --     constant = "#ff9ac1",
          --     -- fun = "#45a9f9",
          --     identifier = "#FFFFFF",
          --     keyword = "#ffcc95",
          --     number = "#ffb86c",
          --     operator = "#FFFFFF",
          --     parameter = "#9a9fd9",
          --     special1 = "#ff9aa9",
          --     special3 = "#ff75b5",
          --     statement = "#ff9ac1",
          --     -- string = "#19f9d8",
          --     -- type = "#ffb86c",
          --     -- variable = "#45a9f9",
          --   },
            ui = {
              bg_gutter = "none",
            },
          -- },
        },
      },
      overrides = function(colors) -- add/modify highlights
        return {
          -- Fun = { fg = "#a5a9f9", italic = false },
          -- String = { fg = "#19f9d8", italic = false },
          -- Type = { fg = "#ffb86c", italic = true },
          -- Variable = { fg = "#45a9f9", italic = false }
        }
      end,
      theme = "dragon", -- Load "wave" theme when 'background' option is not set
      background = { -- map the value of 'background' option to a theme
        dark = "dragon", -- try "dragon" !
        light = "lotus",
      },
    })

    -- vim.cmd.colorscheme("kanagawa")
  end,
}

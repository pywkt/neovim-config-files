return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	keys = {
		{ "<leader>e", "<cmd>Neotree toggle<CR>", desc = "Toggle Neotree" },
		{ "<leader>0", "<cmd>Neotree focus<CR>", desc = "Focus Neotree" }
	},
  opts = {
    filesystem = {
      follow_current_file = { enabled = true },
      use_libuv_file_watcher = true
    },
    window = {
      mappings = {
        ["<space>"] = "none"
      }
    },
    default_component_configs = {
      indent = {
        with_expanders = true,
        expander_collapsed = "∙",
        expander_expanded = "↳",
        expander_highlight = "NeoTreeExpander"
      }
    },
  },
	requires = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		"MunifTanjim/nui.nvim",
	},
}

-- local palette = require("nordic.colors")

return {
  "AlexvZyl/nordic.nvim",
  enabled = false,
  lazy = true,
  priority = 1000,
  config = function()
    require("nordic").setup({
      on_palette = function(palette)
        return palette
      end,
      transparent_bg = true,
      noice = { style = "flat" },
      -- override = {
      -- 	TelescopePrompTitle = {
      -- 		fg = palette.red.bridght,
      -- 		bg = palette.green.base,
      -- 		italic = true,
      -- 		underline = true,
      -- 		sp = palette.yellow.dim,
      -- 		undercurl = false,
      -- 	},
      -- },
    })

    require("nordic").load()
  end,
  -- vim.cmd.colorscheme("nordic"),
}

local home = vim.fn.expand("$HOME")

return {
	"jackMort/ChatGPT.nvim",
	event = "VeryLazy",
	config = function()
		require("chatgpt").setup({
			api_key_cmd = "gpg --decrypt " .. home .. "/.config/nvim/.env/gptkey.txt.gpg",
			openai_params = {
				max_tokens = 1000,
			},
			openai_edit_params = {
				model = "code-davinci-edit-001",
				temperature = 0,
				top_p = 1,
				n = 1,
			},
		})
	end,
	dependencies = {
		"MunifTanjim/nui.nvim",
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope.nvim",
	},
}

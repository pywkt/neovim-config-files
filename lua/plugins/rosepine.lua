return {
  "rose-pine/neovim",
  name = "rose-pine",
  lazy = false, -- make sure we load this during startup if it is your main colorscheme
  priority = 1000, -- make sure to load this before all the other start plugins
  config = function()
    -- Using protected call
    local status_ok, rosepine = pcall(require, "rose-pine")
    if not status_ok then
      return
    end

    rosepine.setup({
      --- @usage 'auto'|'main'|'moon'|'dawn'
      variant = 'moon',
      --- @usage 'main'|'moon'|'dawn'
      dark_variant = 'moon',
      bold_vert_split = false,
      dim_nc_background = false,
      disable_background = true,
      disable_float_background = true,
      disable_italics = false,

      groups = {
        background = 'base',
        background_nc = '_experimental_nc',
        panel = 'surface',
        panel_nc = 'base',
        border = 'pine',
        comment = 'muted',
        link = 'iris',
        punctuation = 'subtle',

        error = 'love',
        hint = 'iris',
        info = 'foam',
        warn = 'gold',

        headings = {
          h1 = 'iris',
          h2 = 'foam',
          h3 = 'rose',
          h4 = 'gold',
          h5 = 'pine',
          h6 = 'foam',
        }
      },
      highlight_groups = {
        ColorColumn = { bg = 'foam', blend = 10 },
        CursorLine = { bg = 'foam', blend = 10 },
        StatusLine = { fg = 'iris', bg = 'pine' },
      }
    })

  -- Set colorscheme after options
    -- vim.cmd.colorscheme("rose-pine")
  end,
}

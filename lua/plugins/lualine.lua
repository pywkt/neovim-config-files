-- return {
--   "nvim-lualine/lualine.nvim",
--   dependencies = {
--     "nvim-tree/nvim-web-devicons",
--     "WhoIsSethDaniel/lualine-lsp-progress",
--   },
--   config = function()
--     require("lualine").setup({
--       options = {
--         icons_enabled = true,
--         theme = "auto",
--         component_separators = "|",
--         section_separators = { left = "", right = "" },
--       },
--       extensions = {
--         "neo-tree",
--         "quickfix",
--         "fzf",
--         "toggleterm",
--       },
--       globalstatus = true,
--       sections = {
--         lualine_a = { "mode" },
--         lualine_b = { "branch", "diff" },
--         lualine_c = { { "filename", file_status = true, path = 1 }, "lsp_progress" },
--         lualine_x = {
--           {
--             require("noice").api.statusline.mode.get,
--             cond = require("noice").api.statusline.mode.has,
--             color = { fg = "#ff9e64" },
--           },
--           {
--             "diagnostics",
--             sources = { "nvim_lsp" },
--             sections = { "error", "warn", "info", "hint" },
--             symbols = { error = "E", warn = "W", info = "I", hint = "H" },
--             colored = true,
--             update_in_insert = false,
--             always_visible = false,
--           },
--         },
--         lualine_y = { "encoding", "fileformat", "filetype", "progress" },
--         lualine_z = {
--           "location",
--           "searchcount",
--           {
--             require("lazy.status").updates,
--             cond = require("lazy.status").has_updates,
--             color = { fg = "#ffffff" },
--           },
--         },
--       },
--       inactive_sections = {},
--       disable_filetypes = { "alpha, dashboard, packer", "neo-tree", "TelescopePrompt" },
--     })
--   end,
-- }
return {
	"nvim-lualine/lualine.nvim",
  event = {"BufRead", "BufNewFile", "ColorScheme" },
	config = function()
		-- Using protected call
		local status_ok, lualine = pcall(require, "lualine")
		if not status_ok then
			return
		end
		local hide_in_width = function()
			return vim.fn.winwidth(0) > 80
		end

		local diagnostics = {
			"diagnostics",
			sources = { "nvim_diagnostic", "nvim_lsp" },
			sections = { "error", "warn" },
			symbols = { error = " ", warn = " " },
			colored = true,
			always_visible = true,
		}

		local diff = {
			"diff",
			colored = true,
			symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
			cond = hide_in_width,
		}

		local filetype = {
			"filetype",
			icons_enabled = true,
		}

		local location = {
			"location",
			padding = 0,
		}

		lualine.setup({
			options = {
				globalstatus = true,
				icons_enabled = true,
				theme = "auto",
				component_separators = { left = "󰿟", right = "󰿟" },
				section_separators = { left = "", right = "" },
				disabled_filetypes = { "alpha", "dashboard" },
				always_divide_middle = true,
			},
			sections = {
				lualine_a = { "mode" },
				lualine_b = { "branch" },
				lualine_c = { diff },
        -- lualine_c = { "filetype" },
        lualine_x = {"filename"},
				lualine_y = { diagnostics, filetype },
				lualine_z = { location },
				-- lualine_z = { "progress" },
			},
		})
	end,
}

return {
	"zbirenbaum/copilot.lua",
	cmd = "Copilot",
  enable = false,
	event = "InsertEnter",
	keys = {
		{ "<leader>ct", "<cmd>Copilot toggle<CR>", desc = "Copilot toggle" },
	},
	opts = {
		panel = {
			enabled = false,
			auto_refresh = false,
			keymap = {
				jump_prev = "[[",
				jump_next = "]]",
				accept = "<CR>",
				refresh = "gr",
				open = "<M-CR>",
			},
			layout = {
				position = "bottom", -- | top | left | right
				ratio = 0.4,
			},
		},
		suggestion = {
			enabled = false,
			auto_trigger = false,
			debounce = 75,
			keymap = {
				accept = "<M-l>",
				dismiss = "<M-h>",
				accept_word = false,
				accept_line = false,
				next = "<M-]>",
				prev = "<M-[>",
			},
		},
    filetypes = {
      ["."] = true,
      go = false,
    },
		copilot_node_command = "node", -- Node.js version must be > 16.x
		server_opts_overrides = {},
	},
}

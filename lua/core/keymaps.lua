-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

--Map leader keys
vim.g.mapleader = " "
vim.b.maplocalleader = ";"

------------- Normal Mode -------------
--Save with Ctrl-S
keymap({ "n", "i" }, "<C-s>", "<cmd>w<CR>")
-- Better move around
keymap("n", "<C-d>", "<C-d>zz")
keymap("n", "<C-u>", "<C-u>zz")
keymap("n", "n", "nzzzv")
keymap("n", "N", "Nzzzv")

-- Better Window Navigation
-- Window Keymap
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-/>", "<C-w>l", opts) -- Focus window to the right
-- Buffer keymap
keymap("n", "<C-H>", "<C-W><C-H>", opts)
keymap("n", "<C-J>", "<C-W><C-J>", opts)
keymap("n", "<C-K>", "<C-W><C-K>", opts)
keymap("n", "<C-L>", "<C-W><C-L>", opts)

-- Better search and highlight
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "*", "*zzzv", opts)
keymap("n", "#", "#zzzv", opts)
keymap("n", "<esc>", "<cmd>noh<cr>", opts)

-- Better paste
keymap("v", "p", '"_dP', opts)
keymap("n", "x", '"_x', opts)
keymap("n", "X", '"_X', opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<Tab>", ":bnext<CR>", opts)
keymap("n", "<S-Tab>", ":bprevious<CR>", opts)

-- Clear highlights
keymap("n", "<leader>h", vim.cmd.nohlsearch, opts)

-- Netrw File Explorer
-- keymap("n", "<leader>.", vim.cmd.Ex, opts)

-- Make current file executable
keymap("n", "<leader><leader>x", "<cmd>!chmod +x %<cr>")

-- _LAZYGIT_TOGGLE
keymap("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opts)

------------- Insert Mode -------------
-- Delete whole word backward
keymap("i", "<C-BS>", "<C-W>", opts)

-- Exit insert mode
keymap("i", "YU", "<C-c>", opts)

-- ChatGPT
keymap("n", "<leader>cy", "<cmd>ChatGPT<CR>", opts)
keymap("n", "<leader>ca", "<cmd>ChatGPTActAs<CR>", opts)
keymap("n", "<leader>cc", "<cmd>ChatGPTCompleteCode<CR>", opts)
keymap("n", "<leader>crt", "<cmd>ChatGPTRun add_tests<CR>", opts)
keymap("n", "<leader>crc", "<cmd>ChatGPTRun complete_code<CR>", opts)
keymap("n", "<leader>crd", "<cmd>ChatGPTRun docstring<CR>", opts)
keymap("n", "<leader>cre", "<cmd>ChatGPTRun explain_code<CR>", opts)
keymap("n", "<leader>crb", "<cmd>ChatGPTRun fix_bugs<CR>", opts)
keymap("n", "<leader>crg", "<cmd>ChatGPTRun grammar_correction<CR>", opts)
keymap("n", "<leader>crk", "<cmd>ChatGPTRun keywords<CR>", opts)
keymap("n", "<leader>cro", "<cmd>ChatGPTRun optimize_code<CR>", opts)
keymap("n", "<leader>ci", "<cmd>ChatGPTEditWithInstructions<CR>", opts)
------------- Visual Mode -------------
-- Stay in indent mode
keymap("x", "<", "<gv", opts)
keymap("x", ">", ">gv", opts)


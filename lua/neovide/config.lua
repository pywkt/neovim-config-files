vim.o.guifont = "Dank_Mono:h11:#e-subpixelantialias:#h-none"
vim.g.neovide_remember_window_size = true
vim.g.neovide_cursor_animation_length = 0.05
vim.g.neovide_cursor_trail_size = 0.1
vim.opt.linespace = 7
